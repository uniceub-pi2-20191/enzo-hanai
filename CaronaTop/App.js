import React from 'react';
import { createStackNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';


import HomeScreen from './source/comp/home.js';
import RegisterScreen from './source/comp/register.js';
import LoginScreen from './source/comp/login.js';
import UserScreen from './source/comp/user.js';
// import DataBase from './source/comp/db.js';

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Register: RegisterScreen,
    Login: LoginScreen,
    User: UserScreen,
    // DB: DataBase, {Não conseguir fazer DataBase}
  },
  {
    initialRouteName: 'Home',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
