import React, { Component } from 'react';
import { AppRegistry, Image, Text, View } from 'react-native';

export default class Bananas extends Component {
  render() {
    const x = {
      uri: 'https://img.stpu.com.br/?img=https://s3.amazonaws.com/pu-mgr/default/a0R0f00000ssfmOEAQ/5aec5b11e4b091444a2c2b77.jpg&w=620&h=400'  
    };
    const y = {
      uri: 'https://i.pinimg.com/originals/09/c2/1b/09c21bfaf5880cdf1d04cde2a74a7038.jpg'
    };

    return (

      <View style={{flex: 1, flexDirection: "column"}}>
        <View style={{ alignItems: "center"}}>
        <Image source={x} style={{width: 375, height: 200, top: 100, left: 0}}/>
        </View>

        <View style={{ alignItems: "center"}}>
        <Text style={{width: 200, height: 100, top: 100, left: 85}}>Sushi</Text>
        </View>

        <View style={{ alignItems: "center"}}>
        <Text style={{width: 200, height: 100, top: 300, left: 80}}>Yakisoba</Text>
        </View>
        
        <View style={{ alignItems: "center"}}>
        <Image source={y} style={{width: 375, height: 200, top: 0, left: 0}}/>
        </View>
      </View>
    );
  }
}